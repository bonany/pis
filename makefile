
default: clean build serve
build:
	mkdocs build -d public

serve:
	mkdocs serve

clean:
	rm -rf site/

push: 
	git add .
	git commit -m "update"
	git push -u origin main