# Analog Processing-In-Memory

Analog PIM macro is a VLSI macro that can perform vector matrix multiplication (VMM) in analog domain, besides the memory read/write function. Analog-Digital converters are required to digitalize the computation results.

!!! Info
    VMM are performed in analog domain with the interaction of voltage/current with the bitcells in APIM.

For research idea introduction visit [B. Yan et al., 2019 Symposium on VLSI Technology](https://ieeexplore.ieee.org/document/8776485)

## Mode/Function

- *Write* : put a piece of data into the memory
- *Read* : fetch data from the memory (given the data is already written into the memory)
- *CIM* : compute-in-memory mode, in the following example, CIM mode perform:

<!-- $$\begin{bmatrix}
cim\_in0 \\
cim\_in1 \\
cim\_in2 \\
cim\_in3 \\
\end{bmatrix}^T
\cdot
\begin{bmatrix}
data_{0,0} & data_{0,1} & \cdots & data_{0,7} \\
data_{1,0} & data_{1,1} & \cdots & data_{1,7} \\
data_{2,0} & data_{2,1} & \cdots & data_{2,7} \\
data_{3,0} & data_{3,1} & \cdots & data_{3,7} \\
\end{bmatrix}
=\begin{bmatrix}
cim\_out0 \\
cim\_out1 \\
\cdots \\
cim\_out7 \\
\end{bmatrix}^T
$$ -->
![APIM address map](img/apimEq.png "APIM Basic Math Equation")

In $data_{i,j}$, $i\in N$ and $j\in N$ are selected from the bulk of the data bitcell matrix by designated *address*.

## Address Map
An example of APIM:

![APIM address map](img/PIMAddressMap.png "APIM address map")

## IO Port

macro global ports:

- port: *clock*
    - input
    - in example code: 1bit
    - macro clock
- port: *en* or *cs*
    - input
    - in example code: 1bit
    - global enable
memory mode ports:
- port: *addr*
    - input
    - in example code: 10bit
    - controlling address
- port: *d*
    - input
    - in example code: 32bit
    - input data port
- port: *q*
    - output
    - in example code: 32bit
    - output data port
- port: *we*
    - input
    - in example code: 1bit
    - write enable, high active

CIM mode ports:

- port: *cme*
    - input
    - in example code: 1bit
    - CIM mode enable, high active
- port: *cmIn* or *cim_in*
    - input
    - in example code: 4bit for each entry; it is a 4-element vector
    - CIM mode input operands
- port: *cmIn* or *cim_out*
    - output
    - in example code: 6bit for each entry (determined by the ADC precision); it is a 8-element vector
    - CIM mode input operands

!!! Tip
    You can always revise the port width per your own need.


## Timing Diagram
!!! Warning
    We only talking about synchronous memory, i.e. the data comes out from "q" (data output port) with 1 clock cycle delay after a rising clock edge captures a deasserted "we" (write enable port)

![Memory timing diagram](img/timingdiagram_apim.svg "Memory timing diagram")

Note: A0, A1, ... are addresses; D0, D1, ... are data.

## Core Code in Verilog

``` verilog
//--------------Internal variables---------------- 
reg [DATA_WIDTH-1:0] mem [0:RAM_DEPTH-1];
//--------------Code Starts Here------------------ 
// Memory Write Block 
// Write Operation : When we = 1, cs = 1
always @ (posedge clk)
begin : MEM_WRITE
   if ( cs && !web ) begin
       mem[a] = d;
   end
end

// Memory Read Block 
// Read Operation : When we = 0, cs = 1
always @ (posedge clk)
begin : MEM_READ
  if (cs && web) begin
    if(cimeb) begin
      q = mem[a];
    end else begin
      // enter CIM mode, this is only behavioral sim
      // actual CIM macro is designed in analog circuits
      cim_out0  = cim_in0 * mem[{2'b00,a[7:5],3'd0,a[1:0]}] 
                + cim_in1 * mem[{2'b01,a[7:5],3'd0,a[1:0]}] 
                + cim_in2 * mem[{2'b10,a[7:5],3'd0,a[1:0]}] 
                + cim_in3 * mem[{2'b11,a[7:5],3'd0,a[1:0]}];
                
      cim_out1  = cim_in0 * mem[{2'b00,a[7:5],3'd1,a[1:0]}] 
                + cim_in1 * mem[{2'b01,a[7:5],3'd1,a[1:0]}] 
                + cim_in2 * mem[{2'b10,a[7:5],3'd1,a[1:0]}] 
                + cim_in3 * mem[{2'b11,a[7:5],3'd1,a[1:0]}];
                
      cim_out2  = cim_in0 * mem[{2'b00,a[7:5],3'd2,a[1:0]}] 
                + cim_in1 * mem[{2'b01,a[7:5],3'd2,a[1:0]}] 
                + cim_in2 * mem[{2'b10,a[7:5],3'd2,a[1:0]}] 
                + cim_in3 * mem[{2'b11,a[7:5],3'd2,a[1:0]}];
                
      cim_out3  = cim_in0 * mem[{2'b00,a[7:5],3'd3,a[1:0]}] 
                + cim_in1 * mem[{2'b01,a[7:5],3'd3,a[1:0]}] 
                + cim_in2 * mem[{2'b10,a[7:5],3'd3,a[1:0]}] 
                + cim_in3 * mem[{2'b11,a[7:5],3'd3,a[1:0]}];
                
      cim_out4  = cim_in0 * mem[{2'b00,a[7:5],3'd4,a[1:0]}] 
                + cim_in1 * mem[{2'b01,a[7:5],3'd4,a[1:0]}] 
                + cim_in2 * mem[{2'b10,a[7:5],3'd4,a[1:0]}] 
                + cim_in3 * mem[{2'b11,a[7:5],3'd4,a[1:0]}];
                
      cim_out5  = cim_in0 * mem[{2'b00,a[7:5],3'd5,a[1:0]}] 
                + cim_in1 * mem[{2'b01,a[7:5],3'd5,a[1:0]}] 
                + cim_in2 * mem[{2'b10,a[7:5],3'd5,a[1:0]}] 
                + cim_in3 * mem[{2'b11,a[7:5],3'd5,a[1:0]}];
                
      cim_out6  = cim_in0 * mem[{2'b00,a[7:5],3'd6,a[1:0]}] 
                + cim_in1 * mem[{2'b01,a[7:5],3'd6,a[1:0]}] 
                + cim_in2 * mem[{2'b10,a[7:5],3'd6,a[1:0]}] 
                + cim_in3 * mem[{2'b11,a[7:5],3'd6,a[1:0]}];
                
      cim_out7  = cim_in0 * mem[{2'b00,a[7:5],3'd7,a[1:0]}] 
                + cim_in1 * mem[{2'b01,a[7:5],3'd7,a[1:0]}] 
                + cim_in2 * mem[{2'b10,a[7:5],3'd7,a[1:0]}] 
                + cim_in3 * mem[{2'b11,a[7:5],3'd7,a[1:0]}];
    end
  end
end

//in this example, the full-precision output should be 14bit
//low-precision ADCs effectively get MSB of the results
assign cim_out0 = cim_out0_tmp[13:13-ADC_PRECISION+1];
assign cim_out1 = cim_out1_tmp[13:13-ADC_PRECISION+1];
assign cim_out2 = cim_out2_tmp[13:13-ADC_PRECISION+1];
assign cim_out3 = cim_out3_tmp[13:13-ADC_PRECISION+1];
assign cim_out4 = cim_out4_tmp[13:13-ADC_PRECISION+1];
assign cim_out5 = cim_out5_tmp[13:13-ADC_PRECISION+1];
assign cim_out6 = cim_out6_tmp[13:13-ADC_PRECISION+1];
assign cim_out7 = cim_out7_tmp[13:13-ADC_PRECISION+1];
```

