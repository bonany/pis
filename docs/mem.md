# Traditional Memory

This is a traditional random-access memory behavioral model.

!!! Info
    A traditional memory support only two operation: *read*, *write*

## Mode/Function


- *Write*: put a piece of data into the memory
- *Read* : fetch data from the memory (given the data is already written into the memory)

## Abstract Diagram
We often use a Turing tape model to describe a memory:
![Memory timing diagram](img/memTape.png "Memory tape model")

## IO Port
- port: *addr*
    - input
    - in example code: 10bit
    - controlling address
- port: *d*
    - input
    - in example code: 32bit
    - input data port
- port: *q*
    - output
    - in example code: 32bit
    - output data port
- port: *we*
    - input
    - in example code: 1bit
    - write enable, high active
- port: *clock*
    - input
    - in example code: 1bit
    - macro clock
- port: *en*
    - input
    - in example code: 1bit
    - global enable

!!! Tip
    You can always revise the port width per your own need.


## Timing Diagram
!!! Warning
    We only talking about synchronous memory, i.e. the data comes out from "q" (data output port) with 1 clock cycle delay after a rising clock edge captures a deasserted "we" (write enable port)

![Memory timing diagram](img/timingdiagram_mem.png "Memory timing diagram")

Note: A0, A1, ... are addresses; D0, D1, ... are data.

## Core Code in Verilog

``` verilog
reg [31:0] mem [10:0];

always @(posedge clock) begin
    if(en) begin
        if(we) begin
            // if write_enable is asserted, push the data into the address
            mem[addr] <= d 
        end
        else begin
            // if write_enable is deasserted, pop the stored data to the port q
            q <= mem[addr]
        end
    end
end
```

