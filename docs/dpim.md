# Digital Processing-In-Memory

Digital PIM macro is an emerging VLSI macro that can perform Hadamard product in digital domain, besides the memory read/write function. Analog-Digital converters are saved from APIM by integrating local processing units (LPU) into the macro.

!!! Info
    VMM are performed in a way of logic gate circuits within the bitcell array in DPIM.

For research idea introduction visit [\[B. Yan, et al. ISSCC'22\]](https://ieeexplore.ieee.org/document/9731545)

## Mode/Function

- *Write* : put a piece of data into the memory
- *Read* : fetch data from the memory (given the data is already written into the memory)
- *CIM* : compute-in-memory mode, in the following example, CIM mode perform:

<!-- $$\begin{bmatrix}
cim\_in0 \\
cim\_in1 \\
\cdots \\
cim\_in31 \\
\end{bmatrix}
\circ
\begin{bmatrix}
data_{0,0}  & \cdots & data_{0,m-1} \\
data_{1,0}  & \cdots & data_{1,m-1} \\
\cdots &  \cdots & \cdots \\
data_{31,0} & \cdots & data_{31,m-1} \\
\end{bmatrix}
= \begin{bmatrix}
cim\_in0\circ data_{0,0}  & \cdots & cim\_in0\circ data_{0,m-1} \\
cim\_in1\circ data_{1,0}  & \cdots & cim\_in0\circ data_{1,m-1} \\
\cdots  & \cdots & \cdots \\
cim\_in31\circ data_{31,0} & \cdots & cim\_in0\circ data_{31,m-1} \\
\end{bmatrix}
$$

$$
=\begin{bmatrix}
cim\_out_{0,0} & \cdots & cim\_out_{0,m-1}  \\
cim\_out_{1,0} & \cdots & cim\_out_{1,m-1}\\
\cdots & \cdots & \cdots\\
cim\_out_{31,0} & \cdots & cim\_out_{31,m-1}\\
\end{bmatrix}
$$ -->
![DPIM address map](img/dpimEq.png "DPIM Basic Math Equation")

- In $data_{i,j}$, $i\in N$ and $j\in N$ are selected from the bulk of the data bitcell matrix by designated *address*
- m could be 1, 2, 3, ... The example code only shows the case of m=1

## Address Map
An example of DPIM:

![DPIM address map](img/DPIM_AddressMap.png "DPIM address map")

## IO Port

macro global ports:

- port: *clock*
    - input
    - in example code: 1bit
    - macro clock
- port: *en* or *cs*
    - input
    - in example code: 1bit
    - global enable
memory mode ports:
- port: *addr*
    - input
    - in example code: 12bit
    - controlling address
- port: *d*
    - input
    - in example code: 32bit
    - input data port
- port: *q*
    - output
    - in example code: 32bit
    - output data port
- port: *we*
    - input
    - in example code: 1bit
    - write enable, high active

CIM mode ports:

- port: *cme*
    - input
    - in example code: 1bit
    - CIM mode enable, high active
- port: *cmIn* or *cim_in*
    - input
    - in example code: 4bit for each entry; it is a 4-element vector
    - CIM mode input operands
- port: *cmIn* or *cim_out*
    - output
    - in example code: 6bit for each entry (determined by the ADC precision); it is a 8-element vector
    - CIM mode input operands

!!! Tip
    You can always revise the port width per your own need.


## Timing Diagram
!!! Warning
    We only talking about synchronous memory, i.e. the data comes out from "q" (data output port) with 1 clock cycle delay after a rising clock edge captures a deasserted "we" (write enable port)

![Memory timing diagram](img/timingdiagram_apim.svg "Memory timing diagram")

Note: A0, A1, ... are addresses; D0, D1, ... are data.

## Core Code in Verilog

``` verilog
//--------------Internal variables---------------- 
reg [DATA_WIDTH-1:0] mem [0:RAM_DEPTH-1];
//--------------Code Starts Here------------------ 
// Memory Write Block 
// Write Operation : When we = 1, cs = 1
always @ (posedge clk)
begin : MEM_WRITE
   if ( cs && !web ) begin
       mem[a] = d;
   end
end

// Memory Read Block 
// Read Operation : When we = 0, oe = 1, cs = 1
always @ (posedge clk)
begin : MEM_READ
  if (cs && web) begin
    if(cimeb) begin
      q = mem[a];
    end else begin
      // enter CIM mode, this is only behavioral sim
      // actual CIM macro is designed with transistor-level circuits
      cim_out = LPU_Func(cim_in, mem)
    end
  end
end

```

