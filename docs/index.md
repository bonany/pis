# Processing-In-Memory in SoC Library

"Processing-In-Memory in SoC" Library (*Pis-Lib*) is a collection that contains typical PIM behavrioral models 

Author: Dr. Bonan Yan

## PIM Macro Behavioral Models

!!! Code
    - Chisel version: [https://gitlab.com/bonany/pislib_chisel](https://gitlab.com/bonany/pislib_chisel)
    - Verilog version: [https://gitlab.com/bonany/pislib_verilog](https://gitlab.com/bonany/pislib_verilog)

## Usage

- Step 1: 

```
git clone https://gitlab.com/bonany/pislib_chisel.git
```
or
```
git clone https://gitlab.com/bonany/pislib_verilog.git
```

- Step 2:
There is *makefile* as the execute guidance

- Step 3:
Explore detailed exemplary models on the top of this page.


<!-- ## Citation

Plese cite this work as:

```
XXX
```

Bibtext:
```
XXX
``` -->


## Reference
1. [B. Yan et al., "Resistive Memory-Based In-Memory Computing: From Device and Large-Scale Integration System Perspectives". Adv. Intell. Syst., 1: 1900068. https://doi.org/10.1002/aisy.201900068](https://onlinelibrary.wiley.com/doi/full/10.1002/aisy.201900068)
2. [B. Yan et al., "A 1.041-Mb/mm2 27.38-TOPS/W Signed-INT8 Dynamic-Logic-Based ADC-less SRAM Compute-in-Memory Macro in 28nm with Reconfigurable Bitwise Operation for AI and Embedded Applications," 2022 IEEE International Solid- State Circuits Conference (ISSCC), 2022, pp. 188-190, doi: 10.1109/ISSCC42614.2022.9731545.](https://ieeexplore.ieee.org/document/9731545)
3. [B. Yan et al., "RRAM-based Spiking Nonvolatile Computing-In-Memory Processing Engine with Precision-Configurable In Situ Nonlinear Activation," 2019 Symposium on VLSI Technology, 2019, pp. T86-T87, doi: 10.23919/VLSIT.2019.8776485.](https://ieeexplore.ieee.org/document/8776485)